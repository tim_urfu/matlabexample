﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MathWorks.MATLAB.NET.Utility;
using MathWorks.MATLAB.NET.Arrays;
using MOptimaGaz;

namespace OptimaGaz
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
        }
      
        private void exitStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button_test_save_Click(object sender, EventArgs e)
        {
            double[] lb = new double[3];
            double[] ub = new double[3];
            double[] f = new double[3];

            // Коэффициенты целевой функции
            f[0] = 2;
            f[1] = 3;
            f[2] = 4;

            // Прямые ограничения на минимальное значение X
            lb[0] = 0;
            lb[1] = 0;
            lb[2] = 0;

            // Прямые ограничения на максимальное значение X
            ub[0] = 10;
            ub[1] = 100;
            ub[2] = 20;

            // Ограничения-неравенства
            double[,] AA = new double[2, 3];

            AA[0, 0] = 1.0;
            AA[0, 1] = 2.0;
            AA[0, 2] = 3.0;

            AA[1, 0] = 2.0;
            AA[1, 1] = 10.0;
            AA[1, 2] = -5.0;

            double[] bb = { 900.0, 950.0 };
            
            MClassOptimaGaz MObject = new MClassOptimaGaz();

            MWArray res = null; //выходной массив метода plane
            MWNumericArray descriptor = null; //массив возвращаемого параметра 

            // MATLAB Execute
            MWArray[] result = MObject.MOptimaGaz(2, (MWNumericArray)f, (MWNumericArray)AA, (MWNumericArray)bb, (MWNumericArray)lb, (MWNumericArray)ub);

            for (int j = 0; j < result.Length; j++)
            {
                descriptor = (MWNumericArray)result[j];

                MessageBox.Show("Вывод массива " + j.ToString());

                double[,] d_descriptor = (double[,])descriptor.ToArray(MWArrayComponent.Real);//преобразование массива MWNUmericArray  к масииву типа double  

                double tmp_sum = 0;
                
                for (int i = 0; i < d_descriptor.Length; i++)//вывод массива d_descriptor в RichBox
                {
                    MessageBox.Show(d_descriptor[i, 0].ToString());

                    tmp_sum += (double) d_descriptor[i, 0];
                }

                MessageBox.Show("Сумма " + j.ToString() + ": " + tmp_sum.ToString());
            }
        }
    }
}
